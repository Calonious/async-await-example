﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AsyncAwait.Shared.Breakfast.Models;

namespace Breakfast.Sample.Async
{
   public class Program
   {
      public static async Task Main(string[] args)
      {
         await RunUnblocked();
         //await RunCompositionTasks();
         //await RunTasksEfficiently();

         Console.ReadKey();
      }

      private static async Task RunUnblocked()
      {
         Coffee cup = PourCoffee();
         Console.WriteLine("coffee is ready");

         Egg eggs = await FryEggs(4);
         Console.WriteLine("eggs are ready");

         Bacon bacon = await FryBacon(6);
         Console.WriteLine("bacon is ready");

         Toast toast = await ToastBread(4);
         ApplyButter(toast);
         ApplyJam(toast);
         Console.WriteLine("toast is ready");

         Juice oj = PourOJ();
         Console.WriteLine("oj is ready");

         Console.WriteLine("Breakfast is ready!");
      }

      private static async Task RunCompositionTasks()
      {
         Coffee cup = PourCoffee();
         Console.WriteLine("coffee is ready");
         Task<Egg> eggsTask = FryEggs(4);
         Task<Bacon> baconTask = FryBacon(6);
         Task<Toast> toastTask = MakeToastWithButterAndJamAsync(4);

         Egg eggs = await eggsTask;
         Console.WriteLine("eggs are ready");

         Bacon bacon = await baconTask;
         Console.WriteLine("bacon is ready");

         Toast toast = await toastTask;
         Console.WriteLine("toast is ready");

         Juice oj = PourOJ();
         Console.WriteLine("oj is ready");

         Console.WriteLine("Breakfast is ready!");

         async Task<Toast> MakeToastWithButterAndJamAsync(int time)
         {
            Toast toast1 = await ToastBread(time);
            ApplyButter(toast1);
            ApplyJam(toast1);
            return toast1;
         }
      }

      private static async Task RunTasksEfficiently()
      {
         Coffee cup = PourCoffee();
         Console.WriteLine("coffee is ready");
         Task<Egg> eggsTask = FryEggs(4);
         Task<Bacon> baconTask = FryBacon(6);
         Task<Toast> toastTask = MakeToastWithButterAndJamAsync(4);

         var allTasks = new List<Task> {eggsTask, baconTask, toastTask};

         while (allTasks.Any())
         {
            Task finishedTask = await Task.WhenAny(allTasks);

            if (finishedTask == eggsTask)
            {
               Console.WriteLine("eggs are ready");
            }

            if (finishedTask == baconTask)
            {
               Console.WriteLine("bacon is ready");
            }

            if (finishedTask == toastTask)
            {
               Console.WriteLine("toast is ready");
            }

            allTasks.Remove(finishedTask);
         }

         Juice oj = PourOJ();
         Console.WriteLine("oj is ready");

         Console.WriteLine("Breakfast is ready!");

         async Task<Toast> MakeToastWithButterAndJamAsync(int time)
         {
            Toast toast1 = await ToastBread(time);
            ApplyButter(toast1);
            ApplyJam(toast1);
            return toast1;
         }
      }

      private static Coffee PourCoffee()
      {
         return new Coffee();
      }

      private static async Task<Egg> FryEggs(int time)
      {
         await CookTime(time);
         return new Egg();
      }

      private static async Task<Bacon> FryBacon(int time)
      {
         await CookTime(time);
         return new Bacon();
      }

      private static async Task<Toast> ToastBread(int time)
      {
         await CookTime(time);
         return new Toast();
      }

      private static void ApplyButter(Toast toast)
      {
         // apply the butter here
      }

      private static void ApplyJam(Toast toast)
      {
         // apply the jam here
      }

      private static Juice PourOJ()
      {
         return new Juice();
      }

      private static async Task CookTime(int time)
      {
         await Task.Delay(TimeSpan.FromSeconds(time));
         
      }
   }
}
