﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait.Shared.Actions
{
   public class BaseTask
   {
      protected Action _action;
      protected Func<Task> _func;

      protected BaseTask(Action action = null, Func<Task> func = null)
      {
         _action = action;
         _func = func;
      }
   }
}
