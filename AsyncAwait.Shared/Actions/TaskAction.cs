﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait.Shared.Actions
{
   public class TaskAction : BaseTask, ITaskAction
   {
      public TaskAction(Action action = null, Func<Task> func = null) : base(action, func)
      {
      }


      public void ExecuteActionSync()
      {
         _action?.Invoke();
      }

      public string ExecuteActionWithReturnSync()
      {
         _action?.Invoke();

         return "Hello World";
      }

      public Task ExecuteAction()
      {
         return Task.Run(() =>
         {
            Console.WriteLine("Executing 'ExecuteAction'");
            _action?.Invoke();
         });
      }

      public Task<string> ExecuteActionWithReturn()
      {
         return Task.Run(() =>
         {
            _func?.Invoke().Wait();
            return "Hello World from Async";
         });
      }

      public async Task<string> ExecuteActionWithReturnAsync()
      {
         Console.WriteLine("Entering Method: ExecuteActionWithReturnAsync");
         return await Task.Run(async () =>
         {
            if (_func != null)
            {
               await _func.Invoke();
            }
            return "Hello World";
         });
      }
   }
}
