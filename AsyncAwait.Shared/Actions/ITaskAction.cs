﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait.Shared.Actions
{
   public interface ITaskAction
   {
      void ExecuteActionSync();

      string ExecuteActionWithReturnSync();

      Task ExecuteAction();

      Task<string> ExecuteActionWithReturn();
   }
}
