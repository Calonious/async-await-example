﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AsyncAwait.Shared.Actions;

namespace Async.Await.Example
{
	public class Program
	{
      // NOTE: Async Task on Console app Main method is only possible with C# 7.1 or greater
		public static async Task Main(string[] args)
      {
         //await RunTaskAction();

         //await RunMultipleTasksChainedAction();

         //await RunMultipleTasksWhenAllAction();

         //await RunMultipleTasksWhenAnyAction();

         Console.WriteLine($"Press any key to end");
			Console.ReadKey();
		}

      private static async Task RunTaskAction()
      {
         TaskAction TaskToRun = new TaskAction(SyncTask);
         Console.WriteLine($"Executing Action on Task");
         await TaskToRun.ExecuteAction();
      }

      private static async Task RunMultipleTasksChainedAction()
      {
         await RunTaskAction().ContinueWith((task) => AsyncTasks());
      }

      private static async Task RunMultipleTasksWhenAllAction()
      {
         Task t1 = AsyncTasks();
         Task t2 = AsyncTasks();
         Task t3 = AsyncTasks();

         await Task.WhenAll(t1, t2, t3);

         Console.WriteLine($"All tasks have completed");
      }

      private static async Task RunMultipleTasksWhenAnyAction()
      {
         Task<string> t1 = AsyncTasksWithReturn("Task 1");
         Task<string> t2 = AsyncTasksWithReturn("Task 2");
         Task<string> t3 = AsyncTasksWithReturn("Task 3");

         Task<string> result = await Task.WhenAny(t1, t2, t3);

         Console.WriteLine($"{result.Result} Completed first");
      }

		private static void SyncTask()
		{
         Random rando = new Random();
         int value = rando.Next(0, 10);
         Console.WriteLine($"Sleeping for {value} sec in SyncTasks");
			Thread.Sleep(1000);
         Console.WriteLine("Sleeping complete on SyncTask");
		}

		private static async Task AsyncTasks()
		{
         Random rando = new Random();
         int value = rando.Next(0, 10);
         Console.WriteLine($"Delaying for {value} seconds in AsyncTasks");

			await Task.Delay(value * 1000);

         Console.WriteLine("Delay Complete for AsyncTasks");
		}

      private static async Task<string> AsyncTasksWithReturn(string name)
      {
         await AsyncTasks();

         return name;
      }

   }
}
