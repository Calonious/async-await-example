﻿using System;
using System.Threading;
using AsyncAwait.Shared.Breakfast.Models;

namespace Breakfast.Sample
{
   public class Program
   {
      public static void Main(string[] args)
      {
         Coffee cup = PourCoffee();
         Console.WriteLine("coffee is ready");

         Egg eggs = FryEggs(2);
         Console.WriteLine("eggs are ready");

         Bacon bacon = FryBacon(3);
         Console.WriteLine("bacon is ready");

         Toast toast = ToastBread(2);
         ApplyButter(toast);
         ApplyJam(toast);
         Console.WriteLine("toast is ready");

         Juice oj = PourOJ();
         Console.WriteLine("oj is ready");

         Console.WriteLine("Breakfast is ready!");

         Console.ReadKey();
      }


      private static Coffee PourCoffee()
      {
         return new Coffee();
      }

      private static Egg FryEggs(int time)
      {
         CookTime(time);
         return new Egg();
      }

      private static Bacon FryBacon(int time)
      {
         CookTime(time);
         return new Bacon();
      }

      private static Toast ToastBread(int time)
      {
         CookTime(time);
         return new Toast();
      }

      private static void ApplyButter(Toast toast)
      {
         // apply the butter here
      }

      private static void ApplyJam(Toast toast)
      {
         // apply the jam here
      }

      private static Juice PourOJ()
      {
         return new Juice();
      }

      private static void CookTime(int time)
      {
         Thread.Sleep(TimeSpan.FromSeconds(time));
      }
   }
}
